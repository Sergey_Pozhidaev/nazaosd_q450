set arduino_dir=C:\Clouds\YandexDisk\Development\tools\arduino-1.6.8
#set arduino_dir=D:\Clouds\YandexDisk\Development\tools\arduino-1.6.8

rd /s /q %CD%\tmp
mkdir %CD%\tmp

%arduino_dir%\arduino-builder -dump-prefs -logger=machine -hardware "%arduino_dir%\hardware" -tools "%arduino_dir%\tools-builder" -tools "%arduino_dir%\hardware\tools\avr" -built-in-libraries "%arduino_dir%\libraries" -libraries "%CD%\libraries" -fqbn=arduino:avr:pro:cpu=16MHzatmega328 -ide-version=10608 -build-path "%CD%\tmp" -warnings=none -prefs=build.warn_data_percentage=75 -verbose "%CD%\minNAZAOSD\minNAZAOSD.ino"
%arduino_dir%\arduino-builder -verbose -compile -logger=machine -hardware "%arduino_dir%\hardware" -tools "%arduino_dir%\tools-builder" -tools "%arduino_dir%\hardware\tools\avr" -built-in-libraries "%arduino_dir%\libraries" -libraries "%CD%\libraries" -fqbn=arduino:avr:pro:cpu=16MHzatmega328 -ide-version=10608 -build-path "%CD%\tmp" -warnings=none -prefs=build.warn_data_percentage=75 -verbose "%CD%\minNAZAOSD\minNAZAOSD.ino"

move %CD%\tmp\minNAZAOSD.ino.hex %CD%\minNAZAOSD.hex
rd /s /q %CD%\tmp
