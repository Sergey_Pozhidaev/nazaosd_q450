#ifndef FRSKYDBUS_H_
#define FRSKYDBUS_H_

#define DBUS_PIN		7

#define DBUS_BAUD     9600

#define DBUS_Header    0x5E
#define DBUS_Tail      0x5E

// Data Ids  http://www.frsky-rc.com/download/down.php?id=126 (Frame Protocol of FrSky Telemetry Hub System (V2.0))
// Official data IDs
#define ID_GPS_Altitude_bp    0x01
#define ID_GPS_Altitude_ap    0x09
#define ID_Temperature1       0x02
#define ID_RPM                0x03
#define ID_Fuel_level         0x04
#define ID_Temperature2       0x05
#define ID_Volt               0x06
#define ID_Altitude_bp        0x10
#define ID_Altitude_ap        0x21
#define ID_GPS_speed_bp       0x11
#define ID_GPS_speed_ap       0x19
#define ID_Longitude_bp       0x12
#define ID_Longitude_ap       0x1A
#define ID_E_W                0x22
#define ID_Latitude_bp        0x13
#define ID_Latitude_ap        0x1B
#define ID_N_S                0x23
#define ID_Course_bp          0x14
#define ID_Course_ap          0x1C
#define ID_Date_Month         0x15
#define ID_Year               0x16
#define ID_Hour_Minute        0x17
#define ID_Second             0x18
#define ID_Acc_X              0x24
#define ID_Acc_Y              0x25
#define ID_Acc_Z              0x26
#define ID_Voltage_Amp_bp     0x3A
#define ID_Voltage_Amp_ap     0x3B
#define ID_Current            0x28
#define ID_VFAS               0x39

void dbus_setup(void);
void dbus_send_cycle(int cnt);
#endif